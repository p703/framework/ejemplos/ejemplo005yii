-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-10-2021 a las 13:01:44
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `ejemplo005yii`
--
CREATE DATABASE IF NOT EXISTS `ejemplo005yii` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `ejemplo005yii`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `coches`
--

DROP TABLE IF EXISTS `coches`;
CREATE TABLE `coches` (
  `id` int(11) NOT NULL,
  `marca` varchar(30) DEFAULT NULL,
  `modelo` varchar(30) DEFAULT NULL,
  `precio` float DEFAULT NULL,
  `fecha_entrada` date DEFAULT NULL,
  `foto` varchar(50) DEFAULT NULL,
  `cilindrada` int(15) DEFAULT NULL,
  `oferta` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `coches`
--

INSERT INTO `coches` (`id`, `marca`, `modelo`, `precio`, `fecha_entrada`, `foto`, `cilindrada`, `oferta`) VALUES
(1, 'Nissan', 'Qashcai', 30000, '2021-09-15', 'nissanqashcai.jpeg', 300, 1),
(2, 'Volkswagen', 'T-Roc', 36000, '2021-09-01', 'volkswagentroc.jpg', 300, 0),
(3, 'Bmv', 'Deia', 55000, '2021-09-09', 'bmvdeia.jpg', 600, 0),
(4, 'Seat', 'Ibiza', 15000, '2021-09-24', 'seatibiza.jpg', 100, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `coches`
--
ALTER TABLE `coches`
  ADD PRIMARY KEY (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
