<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\grid\GridView;
use app\models\Coches;
use yii\data\ActiveDataProvider;


class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()//carga pagina principal
    {
        return $this->render('index');
    }
    
    public function actionCoches() {
        //cargar listado de todos los coches en un gridView
        $dataProvider=new ActiveDataProvider([
            'query'=>Coches::find()
        ]);
        
        return $this->render("coches",[
            'datos'=>$dataProvider,
        ]);
    }
    
    public function actionOfertas() {
       //cargar listado de todos los coches en un listView
        $dataProvider=new ActiveDataProvider([
            'query'=>Coches::find()->where(['oferta'=>1]),
        ]);
        
        return $this->render("ofertas",[
            'dataProvider'=>$dataProvider,
        ]);
    }
    
    public function actionVer($id) {
        //el id es el id del coche del cual quiero ver todos los datos
        
        //consulta utilizando el where con texto
        //$modelo=Coches::find()->where("id=$id")->one();
        //consulta utilizando el where parametrizando
        //$modelo=Coches::find()->where(["id"=>$id])->one();
        
        //consulta utilizando el metodo findOne
        //esto es muy util para buscar por la clave principal
        $modelo=Coches::findOne($id);
        
        return $this->render("ver",[
            "modelo"=>$modelo
        ]);
    }
}